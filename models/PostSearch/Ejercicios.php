<?php

namespace app\models\PostSearch;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ejercicios as EjerciciosModel;

/**
 * Ejercicios represents the model behind the search form of `app\models\Ejercicios`.
 */
class Ejercicios extends EjerciciosModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'series', 'repeticiones', 'tiempo'], 'integer'],
            [['nombre'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EjerciciosModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo' => $this->codigo,
            'series' => $this->series,
            'repeticiones' => $this->repeticiones,
            'tiempo' => $this->tiempo,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }
}
