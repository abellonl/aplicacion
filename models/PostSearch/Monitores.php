<?php

namespace app\models\PostSearch;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Monitores as MonitoresModel;

/**
 * Monitores represents the model behind the search form of `app\models\Monitores`.
 */
class Monitores extends MonitoresModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'aniosexperiencia'], 'integer'],
            [['dni', 'nombrecompleto', 'direccion', 'telefono', 'cuentabancaria'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MonitoresModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'aniosexperiencia' => $this->aniosexperiencia,
        ]);

        $query->andFilterWhere(['like', 'dni', $this->dni])
            ->andFilterWhere(['like', 'nombrecompleto', $this->nombrecompleto])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'cuentabancaria', $this->cuentabancaria]);

        return $dataProvider;
    }
}
