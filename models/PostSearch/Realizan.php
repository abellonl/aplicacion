<?php

namespace app\models\PostSearch;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Realizan as RealizanModel;

/**
 * Realizan represents the model behind the search form of `app\models\Realizan`.
 */
class Realizan extends RealizanModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'estado', 'codsesion', 'codespacio', 'idmonitor'], 'integer'],
            [['hora'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RealizanModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'hora' => $this->hora,
            'estado' => $this->estado,
            'codsesion' => $this->codsesion,
            'codespacio' => $this->codespacio,
            'idmonitor' => $this->idmonitor,
        ]);

        return $dataProvider;
    }
}
