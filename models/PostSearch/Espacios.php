<?php

namespace app\models\PostSearch;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Espacios as EspaciosModel;

/**
 * Espacios represents the model behind the search form of `app\models\Espacios`.
 */
class Espacios extends EspaciosModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'maxcapacidad'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EspaciosModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigo' => $this->codigo,
            'maxcapacidad' => $this->maxcapacidad,
        ]);

        return $dataProvider;
    }
}
