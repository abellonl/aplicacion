<?php

namespace app\models;
use Yii;

/**
 * This is the model class for table "sesiones".
 *
 * @property int $id
 * @property string $hora
 * @property int $estado
 * @property int $codclase
 * @property int $codespacio
 * @property int $idmonitor
 *
 * @property Apuntarse[] $apuntarses
 * @property Clases $codclase0
 * @property Espacios $codespacio0
 * @property Monitores $idmonitor0
 */
class Sesiones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sesiones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hora'], 'required', 'message' => 'Este campo es obligatorio.'],
            [['codclase'], 'required', 'message' => 'Este campo es obligatorio.'],
            [['codespacio'], 'required', 'message' => 'Este campo es obligatorio.'],
            [['idmonitor'], 'required', 'message' => 'Este campo es obligatorio'],
            [['codclase', 'codespacio', 'idmonitor'], 'integer'],
            [['codclase'], 'exist', 'skipOnError' => true, 'targetClass' => Clases::class, 'targetAttribute' => ['codclase' => 'codigo']],
            [['codespacio'], 'exist', 'skipOnError' => true, 'targetClass' => Espacios::class, 'targetAttribute' => ['codespacio' => 'codigo']],
            [['idmonitor'], 'exist', 'skipOnError' => true, 'targetClass' => Monitores::class, 'targetAttribute' => ['idmonitor' => 'id']],
            [['hora', 'codespacio'], 'unique', 'targetAttribute' => ['hora', 'codespacio'], 'message' => 'A esta hora la sala está reservada para otra sesión.'],
            [['hora', 'idmonitor'], 'unique', 'targetAttribute' => ['hora', 'idmonitor'], 'message' => 'A esta hora el monitor está asignado a otra sesión.'],
            
            //[['hora'], 'match', 'pattern' => '/^([01]?[0-9]|2[0-3]):[0-5][0-9]$/', 'message' => 'El formato de la hora debe ser hh:mm'],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hora' => 'Hora',
            'codclase' => 'Código Clase',
            'codespacio' => 'Sala',
            'idmonitor' => 'ID Monitor',
        ];
    }
    

    /**
     * Gets query for [[Apuntarse]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApuntarse()
    {
        return $this->hasMany(Apuntarse::class, ['idsesion' => 'id']);
    }
    
    /**
     * Gets query for [[Codclase]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodclase()
    {
        return $this->hasOne(Clases::class, ['codigo' => 'codclase']);
    }

    /**
     * Gets query for [[Codespacio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodespacio()
    {
        return $this->hasOne(Espacios::class, ['codigo' => 'codespacio']);
    }

    /**
     * Gets query for [[Idmonitor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdmonitor()
    {
        return $this->hasOne(Monitores::class, ['id' => 'idmonitor']);
    }
    
}
