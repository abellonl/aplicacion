<?php

namespace app\models;

/**
 * This is the model class for table "componen".
 *
 * @property int $id
 * @property int|null $codrutina
 * @property int|null $codejercicio
 *
 * @property Ejercicios $codejercicio0
 * @property Rutinas $codrutina0
 */
class Componen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'componen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codrutina', 'codejercicio'], 'integer'],
            [['codejercicio'], 'exist', 'skipOnError' => true, 'targetClass' => Ejercicios::class, 'targetAttribute' => ['codejercicio' => 'codigo']],
            [['codrutina'], 'exist', 'skipOnError' => true, 'targetClass' => Rutinas::class, 'targetAttribute' => ['codrutina' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codrutina' => 'Código Rutina',
            'codejercicio' => 'Código Ejercicio',
        ];
    }

    /**
     * Gets query for [[Codejercicio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodejercicio()
    {
        return $this->hasOne(Ejercicios::class, ['codigo' => 'codejercicio']);
    }

    /**
     * Gets query for [[Codrutina]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodrutina()
    {
        return $this->hasOne(Rutinas::class, ['codigo' => 'codrutina']);
    }
}
