<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $id
 * @property string $dni
 * @property string $nombrecompleto
 * @property string $direccion
 * @property string $telefono
 * @property string $cuentabancaria
 * @property int|null $identrenadorpersonal
 *
 * @property Apuntarse[] $apuntarses
 * @property Entrenadorespersonales $identrenadorpersonal0
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required', 'message' => 'Este campo es obligatorio.'],
            [['nombrecompleto'], 'required', 'message' => 'Este campo es obligatorio.'],
            [['direccion'], 'required', 'message' => 'Este campo es obligatorio.'],
            [['telefono'], 'required', 'message' => 'Este campo es obligatorio'],
            [['cuentabancaria'], 'required', 'message' => 'Este campo es obligatorio.'],
            [['dni'], 'string', 'max' => 9],
            [['nombrecompleto'], 'string', 'max' => 60, 'min' => '3', 'tooShort' => 'El nombre debe tener como minimo 3 caracteres.', 'tooLong' => 'El nombre puede tener como máximo 60 caracteres.'],
            [['direccion'], 'string', 'max' => 100, 'min' => '10', 'tooShort' => 'La dirección debe tener como minimo 10 caracteres.', 'tooLong' => 'La dirección puede tener como máximo 120 caracteres.'],
            [['telefono'], 'string', 'max' => 9, 'tooLong' => 'Formato incorrecto. Solo se admiten teléfonos móviles de España (666777888).'],
            [['cuentabancaria'], 'string', 'max' => 28],
            [['dni'], 'unique'],
            [['identrenadorpersonal'], 'safe'],

            [['dni'], 'match', 'pattern' => '/^([0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKE])|([XYZ][0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKE])$/i', 'message' => 'Formato incorrecto (DNI: 12345678A / NIE: X1234567A).'],
            [['cuentabancaria'], 'match', 'pattern' => '/^ES|es\d{2}-\d{4}-\d{4}-\d{2}-\d{10}$/', 'message' => 'Formato incorrecto (ES55-4444-3333-22-1234567890).'],
            [['telefono'], 'match', 'pattern' => '/^[6-8]\d{8}$/', 'message' => 'Formato incorrecto. Solo se admiten teléfonos móviles de España (666777888).'],
            [['nombrecompleto'], 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚüÜñÑºª\s-]+$/i', 'message' => 'Formato incorrecto. Solo se admiten letras.'],
            ['dni', 'validateDNI', 'message' => 'Formato incorrecto. La letra es incorrecta.'],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dni' => 'DNI/NIE',
            'nombrecompleto' => 'Nombre Completo',
            'direccion' => 'Dirección',
            'telefono' => 'Teléfono',
            'cuentabancaria' => 'Cuenta Bancaria',
            'identrenadorpersonal' => 'ID Entrenador Personal',
        ];
    }
    
    public function validateDNI($attribute, $params)
    {
        $letras = "TRWAGMYFPDXBNJZSQVHLCKE";
        $dni = strtoupper($this->$attribute);

        if (preg_match('/^([0-9]{8})([A-Za-z])$/', $dni, $matches)) {
            // Es un DNI
            $numero = $matches[1];
            $letra = $matches[2];
        } elseif (preg_match('/^([XYZ])([0-9]{7})([A-Za-z])$/', $dni, $matches)) {
            // Es un NIE
            $prefijo = $matches[1];
            $numero = $matches[2];
            $letra = $matches[3];

            // Convertir la letra inicial en su equivalente numérico
            switch ($prefijo) {
                case 'X':
                    $numero = '0' . $numero;
                    break;
                case 'Y':
                    $numero = '1' . $numero;
                    break;
                case 'Z':
                    $numero = '2' . $numero;
                    break;
            }
        } else {
            $this->addError($attribute, 'El DNI/NIE no es válido.');
            return;
        }

        // Validar la letra de control
        $letra_correcta = $letras[$numero % 23];
        if ($letra !== $letra_correcta) {
            $this->addError($attribute, 'El DNI/NIE no es válido. La letra de control no coincide.');
        }
    }

    
    /*
     * DNI/NIE y CuentaBancaria a mayúscula
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->dni = strtoupper($this->dni);
            $this->cuentabancaria = strtoupper($this->cuentabancaria);
            return true;
        }
        return false;
    }

    /*
     * Eliminar espacios
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->nombrecompleto = trim($this->nombrecompleto);
            $this->direccion = trim($this->direccion);
            return true;
        }
        return false;
    }

    /**
     * Gets query for [[Apuntarse]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApuntarse()
    {
        return $this->hasMany(Apuntarse::class, ['idcliente' => 'id']);
    }

    /**
     * Gets query for [[Identrenadorpersonal]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdentrenadorpersonal()
    {
        return $this->hasOne(Entrenadorespersonales::class, ['id' => 'identrenadorpersonal']);
    }
    
}
