<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "apuntarse".
 *
 * @property int $codigo
 * @property int $idcliente
 * @property int $idsesion
 * @property int|null $asistencia
 *
 * @property Clientes $idcliente0
 * @property Sesiones $idsesion0
 */
class Apuntarse extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apuntarse';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idcliente', 'idsesion'], 'required'],
            [['idcliente', 'idsesion', 'asistencia'], 'integer'],
            [['idcliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::class, 'targetAttribute' => ['idcliente' => 'id']],
            [['idsesion'], 'exist', 'skipOnError' => true, 'targetClass' => Sesiones::class, 'targetAttribute' => ['idsesion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Código',
            'idcliente' => 'ID Cliente',
            'idsesion' => 'ID Sesión',
            'asistencia' => 'Asistencia',
        ];
    }
    

    /**
     * Gets query for [[Idcliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcliente()
    {
        return $this->hasOne(Clientes::class, ['id' => 'idcliente']);
    }

    /**
     * Gets query for [[Idsesion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdsesion()
    {
        return $this->hasOne(Sesiones::class, ['id' => 'idsesion']);
    }
    
    public function getCodClase()
    {
        return $this->hasOne(Clases::class, ['codigo' => 'codclase'])->via('idsesion');
    }

}
