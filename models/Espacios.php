<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "espacios".
 *
 * @property int $codigo
 * @property int $maxcapacidad
 *
 * @property Sesiones[] $sesiones
 */
class Espacios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'espacios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['maxcapacidad'], 'required'],
            [['maxcapacidad'], 'integer'],

            [['maxcapacidad'], 'integer', 'max' => 50, 'min' => 10, 'tooBig' => 'Máximo de capacidad permitido es 50.', 'tooSmall' => 'Mínimo de capacidad permitido es 10.', 'message' => 'Este campo no admite decimales.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Sala',
            'maxcapacidad' => 'Máxima Capacidad',
        ];
    }

    /**
     * Gets query for [[Sesiones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSesiones()
    {
        return $this->hasMany(Sesiones::class, ['codespacio' => 'codigo']);
    }
}
