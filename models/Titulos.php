<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "titulos".
 *
 * @property int $codigo
 * @property int|null $identrenadorpersonal
 * @property string|null $titulo
 *
 * @property Entrenadorespersonales $identrenadorpersonal0
 */
class Titulos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'titulos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['identrenadorpersonal'], 'integer'],
            [['titulo'], 'string', 'max' => 80],
            [['identrenadorpersonal', 'titulo'], 'unique', 'targetAttribute' => ['identrenadorpersonal', 'titulo']],
            [['identrenadorpersonal'], 'exist', 'skipOnError' => true, 'targetClass' => Entrenadorespersonales::class, 'targetAttribute' => ['identrenadorpersonal' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Código',
            'identrenadorpersonal' => 'ID Entrenador Personal',
            'titulo' => 'Título',
        ];
    }

    /**
     * Gets query for [[Identrenadorpersonal]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdentrenadorpersonal()
    {
        return $this->hasOne(Entrenadorespersonales::class, ['id' => 'identrenadorpersonal']);
    }
}
