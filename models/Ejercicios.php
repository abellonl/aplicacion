<?php

namespace app\models;


/**
 * This is the model class for table "ejercicios".
 *
 * @property int $codigo
 * @property string|null $nombre
 * @property int|null $series
 * @property int|null $repeticiones
 * @property int|null $tiempo
 *
 * @property Componen[] $componens
 */
class Ejercicios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ejercicios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required', 'message' => 'Este campo es obligatorio.'],
            [['series'], 'required', 'message' => 'Este campo es obligatorio.'],
            [['repeticiones'], 'required', 'message' => 'Este campo es obligatorio.'],
            [['tiempo'], 'required', 'message' => 'Este campo es obligatorio.'],
            [['series', 'repeticiones', 'tiempo'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
            
            [['nombre'], 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚüÜñÑ\s-]+$/i', 'message' => 'Formato incorrecto. Solo se admiten letras.'],
            [['series'], 'integer', 'max' => 6, 'min' => 0, 'tooBig' => 'Máximo de número de series permitido es 6.', 'tooSmall' => 'No se admiten números negativos.', 'message' => 'Este campo no admite decimales.'],
            [['repeticiones'], 'integer', 'max' => 20, 'min' => 0, 'tooBig' => 'Máximo de número de repeticiones permitido es 20.', 'tooSmall' => 'No se admiten números negativos.', 'message' => 'Este campo no admite decimales.'],
            [['tiempo'], 'integer', 'max' => 90, 'min' => 0, 'tooBig' => 'Máximo de minutos permitido es 90.', 'tooSmall' => 'No se admiten números negativos.', 'message' => 'Este campo no admite decimales.']
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Código',
            'nombre' => 'Nombre',
            'series' => 'Series',
            'repeticiones' => 'Repeticiones',
            'tiempo' => 'Tiempo',
        ];
    }

    /**
     * Gets query for [[Componen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComponen()
    {
        return $this->hasMany(Componen::class, ['codejercicio' => 'codigo']);
    }
    
}
