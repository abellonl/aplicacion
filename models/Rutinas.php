<?php

namespace app\models;

/**
 * This is the model class for table "rutinas".
 *
 * @property int $codigo
 * @property string|null $tipo
 * @property int|null $identrenadorpersonal
 *
 * @property Componen[] $componens
 * @property Entrenadorespersonales $identrenadorpersonal0
 */
class Rutinas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rutinas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['identrenadorpersonal'], 'integer'],
            [['tipo'], 'string', 'max' => 50],
            [['identrenadorpersonal'], 'exist', 'skipOnError' => true, 'targetClass' => Entrenadorespersonales::class, 'targetAttribute' => ['identrenadorpersonal' => 'id']],
            
            [['tipo'], 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚüÜñÑ\s-]+$/i', 'message' => 'Formato incorrecto. Solo se admiten letras.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Código',
            'tipo' => 'Tipo',
            'identrenadorpersonal' => 'ID Entrenador Personal',
        ];
    }

    /**
     * Gets query for [[Componen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComponen()
    {
        return $this->hasMany(Componen::class, ['codrutina' => 'codigo']);
    }

    /**
     * Gets query for [[Identrenadorpersonal]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdentrenadorpersonal()
    {
        return $this->hasOne(Entrenadorespersonales::class, ['id' => 'identrenadorpersonal']);
    }
    
}
