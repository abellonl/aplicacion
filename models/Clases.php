<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clases".
 *
 * @property int $codigo
 * @property string $actividad
 *
 * @property Sesiones[] $sesiones
 */
class Clases extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clases';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['actividad'], 'required'],
            [['actividad'], 'string', 'max' => 55],

            [['actividad'], 'match', 'pattern' => '/^[a-zA-ZáéíóúÁÉÍÓÚüÜñÑ()\s-]+$/i', 'message' => 'Formato incorrecto. Solo se admiten letras.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Código',
            'actividad' => 'Actividad',
        ];
    }

    /**
     * Gets query for [[Sesiones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSesiones()
    {
        return $this->hasMany(Sesiones::class, ['codclase' => 'codigo']);
    }
}
