<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Entrenadorespersonales $model */
$this->title = $model->nombrecompleto;

\yii\web\YiiAsset::register($this);
?>
<div class="entrenadorespersonales-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro de eliminar este registro?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'dni',
            'nombrecompleto',
            'direccion',
            'telefono',
            'cuentabancaria',
            [
                'label' => 'Títulos',
                'value' => function ($model) {
                    $titulos = '';
                    if (!empty($model->titulos)) {
                        foreach ($model->titulos as $titulo) {
                            $titulos .= Html::encode($titulo->titulo) . '<br>';
                        }
                    } else {
                        $titulos = 'Sin títulos asignados';
                    }
                    return $titulos;
                },
                'format' => 'html',
            ],
        ],
    ])
    ?>


</div>
