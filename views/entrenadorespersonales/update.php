<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Entrenadorespersonales $model */

$this->title = 'Actualizando Entrenador Personal';

?>
<div class="entrenadorespersonales-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
