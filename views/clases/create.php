<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Clases $model */

$this->title = 'Nueva Clase';

?>
<div class="clases-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
