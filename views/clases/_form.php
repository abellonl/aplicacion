<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Clases $model */
/** @var yii\widgets\ActiveForm $form */
?>

<p>
    <?= Html::a('Volver', ['volver'], ['class' => 'btn btn-secondary']) ?>
</p>

<div class="clases-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'actividad')->textInput(['maxlength' => true, 'placeholder' => 'Introduzca el nombre de la actividad...']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>