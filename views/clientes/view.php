<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Clientes $model */

$this->title = $model->nombrecompleto;
$deleteUrl = \yii\helpers\Url::to(['delete', 'id' => $model->id]);
$this->registerJs("var deleteUrl = '{$deleteUrl}';", \yii\web\View::POS_HEAD);

?>
<div class="clientes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro de eliminar este registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'dni',
            'nombrecompleto',
            'direccion',
            'telefono',
            'cuentabancaria',
            [
                'label' => 'Entrenador Personal',
                'value' => function ($model) {
                    $entrenadorPersonal = $model->getIdentrenadorpersonal()->one();
                    if ($entrenadorPersonal !== null) {
                        return $entrenadorPersonal->nombrecompleto;
                    } else {
                        return 'Sin asignar';
                    }
                },
            ],
        ],
    ]) ?>

</div>
