<?php

use app\models\Clientes;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\PostSearch\Clientes $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Clientes';
?>
<div class="clientes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nuevo Cliente', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'summary' => '',
    'columns' => [
        [
            'attribute' => 'dni',
            'headerOptions' => ['style' => 'width: 100px;'],
        ],
        [
            'attribute' => 'nombrecompleto',
            'headerOptions' => ['style' => 'width: 125px;'],
        ],
        [
            'attribute' => 'direccion',
            'headerOptions' => ['style' => 'width: 250px;'],
        ],
        [
            'attribute' => 'telefono',
            'headerOptions' => ['style' => 'width: 100px;'],
        ],
        [
            'attribute' => 'cuentabancaria',
            'headerOptions' => ['style' => 'width: 200px;'],
        ],
        [
            'class' => ActionColumn::className(),
            'urlCreator' => function ($action, Clientes $model, $key, $index, $column) {
                return Url::toRoute([$action, 'id' => $model->id]);
            },
            'headerOptions' => ['style' => 'width: 60px;'],
        ],
    ],
]); ?>



</div>
