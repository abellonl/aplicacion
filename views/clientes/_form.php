<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Clientes $model */
/** @var yii\widgets\ActiveForm $form */

?>

<p>
    <?= Html::a('Volver', ['volver'], ['class' => 'btn btn-secondary']) ?>
</p>

<div class="clientes-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'dni')->textInput(['maxlength' => true, 'placeholder' => 'Introduzca su DNI/NIE...', 'autocomplete' => 'off']) ?>
        </div>
        <div class="col-md-8">
            <?= $form->field($model, 'nombrecompleto')->textInput(['maxlength' => true, 'placeholder' => 'Introduzca su nombre y apellidos...', 'autocomplete' => 'off']) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'cuentabancaria')->textInput(['maxlength' => true, 'placeholder' => 'Introduzca el número de su cuenta bancaria...', 'autocomplete' => 'off']) ?>
        </div>
        <div class="col-md-8">
            <?= $form->field($model, 'telefono')->textInput(['maxlength' => true, 'placeholder' => 'Introduzca su número de teléfono...', 'type' => 'number', 'id' => 'telefono-input', 'autocomplete' => 'off']) ?>
        </div>
    </div>
    
    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true, 'placeholder' => 'Introduzca su dirección...', 'autocomplete' => 'off']) ?>
    
    <?= $form->field($model, 'identrenadorpersonal')->dropDownList(
        \yii\helpers\ArrayHelper::map(
            \app\models\Entrenadorespersonales::find()->all(),
            'id',
            function ($entrenador) {
                return $entrenador->nombrecompleto;
            }
        ),
        ['prompt' => 'Selecciona un entrenador personal']
    )->label('Entrenador Personal') ?>
    
    
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
