<?php
use yii\helpers\Html;

/* @var $model app\models\Entrenadorespersonales */

$clientes = $model->clientes;
?>

<div class="entrenador-cliente">
    <div class="entrenador">
        <h4><?=Html::encode($model->nombrecompleto)?></h4>
        <h5><?='Clientes Asignados: ' . count($clientes)?></h5>
        <ul class="despl-clientes">
            
            <?php if (!empty($clientes)): ?>

                <?php foreach ($clientes as $cliente): ?>
                    <li>
                        <?= Html::a(
                            Html::encode($cliente->nombrecompleto),
                            ['clientes/view', 'id' => $cliente->id],
                            ['class' => 'a-clientes']
                        ) ?>
                    </li>
                <?php endforeach; ?>

            <?php else: ?>
                <p>No hay clientes asignados.</p>
            <?php endif; ?>
        </ul>   
    </div>
</div>
