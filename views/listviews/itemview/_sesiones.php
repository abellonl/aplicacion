<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Monitores;
use app\models\Clases;
use app\models\Apuntarse;
use app\models\Espacios;

/* @var $model app\models\Sesiones */
/* @var $monitores app\models\Monitores */
/* @var $apuntes int */

$monitor = Monitores::findOne($model->idmonitor);
$clase = Clases::findOne($model->codclase);
$apuntes = Apuntarse::findOne($model->apuntarse);
$espacio = Espacios::findOne($model->codespacio);
$countApuntes = Apuntarse::find()->where(['idsesion' => $model->id])->count();
$apuntarseUrl = Url::to(['apuntarse/create', 'id' => $model->id]);

?>

<body>
    
    <div class="sesiones" data-plazas-disponibles="<?= Html::encode($espacio->maxcapacidad) - $countApuntes ?>">
        <div class="overlay"></div>
        <div class="sesion">
            <h4><?= Html::encode($clase->actividad) ?></h4>
            <h5><?= 'Hora: ' . Html::encode($model->hora) ?></h5>
            <h5><?= 'Sala: ' . Html::encode($espacio->codigo) ?></h5>
            <h5><?= 'Monitor: ' . Html::encode($monitor->nombrecompleto) ?></h5>
            <div class="plazas-disponibles">
                <h5><?= 'Plazas Disponibles: ' . (Html::encode($espacio->maxcapacidad) - $countApuntes) . '/' . Html::encode($espacio->maxcapacidad) ?></h5>
                <?= Html::a('Apuntar', ['apuntarse/create', 'id' => $model->id], ['class' => 'apuntar-btn']) ?>
            </div>
        </div>
    </div>
    
    
    <script>
    document.addEventListener('DOMContentLoaded', function() {
        var tarjetas = document.querySelectorAll('.sesiones');

        tarjetas.forEach(function(tarjeta) {
            var plazasDisponibles = parseInt(tarjeta.getAttribute('data-plazas-disponibles'), 10);
            console.log('Plazas Disponibles:', plazasDisponibles);  // Mensaje de depuración

            var apuntarBtn = tarjeta.querySelector('.aountar-btn');

            if (plazasDisponibles === 0) {
                tarjeta.classList.add('disabled');
                apuntarBtn.disabled = true;
                console.log('Tarjeta deshabilitada');  // Mensaje de depuración
            }
        });
    });
    </script>
       
</body>