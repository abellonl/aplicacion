<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sesiones de Hoy';
?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="body-content mt-5">
    
    <div class="row">
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '/listviews/itemview/_sesiones', // Ruta del itemView
            'layout' => "{items}",
            'options' => ['class' => 'row'],
            'itemOptions' => ['class' => 'col-sm-4 mb-3'],
        ]); ?>
    </div>
</div>
