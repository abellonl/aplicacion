<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Entrenadores personales';
?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="body-content mt-5">
    
    <div class="row">
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '/listviews/itemview/_entrenadoresclientes', // Ruta de la vista parcial
            'layout' => "{items}",
            'options' => ['class' => 'row'],
            'itemOptions' => ['class' => 'col-sm-3 mb-3'],
        ]); ?>
    </div>
</div>

