<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);

$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::getAlias('@web/favicon.ico')]);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/images/logo.png', [
        'alt' => 'Logo',
        'class' => 'logo',
        'style' => 'width: 65px; border-bottom-width: 20px;',
        ]) . ' ' . Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
        ['label' => 'Información', 'items' => [
            ['label' => 'Entrenadores', 'url' => ['/entrenadorespersonales/entrenadoresclientes']],
            ['label' => 'Sesiones del Día', 'url' => ['/sesiones/versesiones']],
            ['label' => 'Estadísticas', 'url' => ['/estadisticas/index']],
        ]],
        
        ['label' => 'Trabajadores', 'items' => [
            ['label' => 'Entrenadores Personales', 'url' => ['/entrenadorespersonales/index']],
            ['label' => 'Monitores', 'url' => ['/monitores/index']],
        ]],
            
        ['label' => 'Servicio al Cliente', 'items' => [
           ['label' => 'Clientes', 'url' => ['/clientes/index']],
           ['label' => 'Clientes Apuntados', 'url' => ['/apuntarse/index']],
        ]],
        
        ['label' => 'Gestión Clases', 'items' => [
           ['label' => 'Sesiones', 'url' => ['/sesiones/index']],
           ['label' => 'Clases', 'url' => ['/clases/index']],
           ['label' => 'Espacios', 'url' => ['/espacios/index']],
        ]],
        
        ['label' => 'Rutinas y Ejercicios', 'items' => [
           ['label' => 'Rutinas', 'url' => ['/rutinas/index']],
           ['label' => 'Ejercicios', 'url' => ['/ejercicios/index']],
           ['label' => 'Asignar Ejercicios', 'url' => ['/componen/index']],
        ]],
        
        ],
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-1">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; RealFit <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
