<?php

use app\models\Monitores;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\PostSearch\Monitores $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Monitores';

?>
<div class="monitores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nuevo Monitor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => '',
        'columns' => [
        [
            'attribute' => 'dni',
            'headerOptions' => ['style' => 'width: 125px;'],
        ],
        [
            'attribute' => 'nombrecompleto',
            'headerOptions' => ['style' => 'width: 150px;'],
        ],
        [
            'attribute' => 'direccion',
            'headerOptions' => ['style' => 'width: 250px;'],
        ],
        [
            'attribute' => 'telefono',
            'headerOptions' => ['style' => 'width: 75px;'],
        ],   
        [
            'attribute' => 'cuentabancaria',
            'headerOptions' => ['style' => 'width: 200px;'],
        ],
		        [
            'attribute' => 'aniosexperiencia',
            'headerOptions' => ['style' => 'width: 100px;'],
        ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Monitores $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                },
                'headerOptions' => ['style' => 'width: 60px;'],
            ],
        ],
    ]); ?>


</div>
