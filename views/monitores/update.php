<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Monitores $model */

$this->title = 'Actualizando Monitor';

?>
<div class="monitores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>