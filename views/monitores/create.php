<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Monitores $model */

$this->title = 'Crear Monitor';

?>
<div class="monitores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
