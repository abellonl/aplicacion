<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Titulos $model */
/** @var yii\widgets\ActiveForm $form */
/** @var array $titulos Array de modelos Titulos existentes */
?>

<p>
    <?= Html::a('Volver', ['volver'], ['class' => 'btn btn-secondary']) ?>
</p>

<div class="titulos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombrecompleto')->textInput(['maxlength' => true, 'readonly' => true, 'autocomplete' => 'off'])->label('Entrenador Personal') ?>
    
    <?php foreach ($titulos as $index => $titulo): ?>
        <?= $form->field($titulo, "[$index]titulo")->dropDownList(
            \yii\helpers\ArrayHelper::map(
                \app\models\Titulos::find()->distinct()->all(),
                'titulo',
                function ($titulo) {
                    return $titulo->titulo;
                }
            ),
            ['prompt' => 'Selecciona un título']
        )->label("Título " . ($index + 1)) ?>
    <?php endforeach; ?>

    <!-- Asegurarse de que haya al menos dos formularios de título -->
    <?php for ($i = count($titulos); $i < max(1, count($titulos) + 1); $i++): ?>
        <?php $titulonuevo = new \app\models\Titulos(); ?>
        <?= $form->field($titulonuevo, "[$i]titulo")->dropDownList(
            \yii\helpers\ArrayHelper::map(
                \app\models\Titulos::find()->distinct()->all(),
                'titulo',
                function ($titulo) {
                    return $titulo->titulo;
                }
            ),
            ['prompt' => 'Selecciona un título']
        )->label("Título " . ($i + 1)) ?>
    <?php endfor; ?>
    
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
