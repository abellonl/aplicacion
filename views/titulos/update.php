<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Titulos $model */

$this->title = 'Actualizando Entrenador Personal';

?>
<div class="titulos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'titulos' => $titulos,
    ]) ?>

</div>
