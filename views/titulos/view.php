<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Titulos $model */

$this->title = 'ID Entrenador Personal: ' . $model->identrenadorpersonal;

\yii\web\YiiAsset::register($this);
?>
<div class="titulos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'codigo' => $model->codigo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'codigo' => $model->codigo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro de eliminar este registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'identrenadorpersonal',
            [
                'label' => 'Entrenador Personal',
                'value' => function ($model) {
                    $entrenadorPersonal = $model->getIdentrenadorpersonal()->one();
                        return $entrenadorPersonal->nombrecompleto;
                },
            ],
            'titulo',
        ],
    ]) ?>

</div>
