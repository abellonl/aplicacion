<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Titulos $model */

$this->title = 'Crear Entrenador Personal';

?>
<div class="titulos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'identrenadorpersonal')->textInput(['maxlength' => true, 'readonly' => true, 'autocomplete' => 'off'])->label('Entrenador Personal') ?>

    <?= $form->field($model, 'titulo[]')->dropDownList(
        \yii\helpers\ArrayHelper::map(
            \app\models\Titulos::find()->distinct()->all(),
            'titulo',
            function ($titulo) {
                return $titulo->titulo;
            }
        ),
        ['prompt' => 'Selecciona un título']
    )->label('Título 1') ?>
    
    <?= $form->field($model, 'titulo[]')->dropDownList(
        \yii\helpers\ArrayHelper::map(
            \app\models\Titulos::find()->distinct()->all(),
            'titulo',
            function ($titulo) {
                return $titulo->titulo;
            }
        ),
        ['prompt' => 'Selecciona un título']
    )->label('Título 2') ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

