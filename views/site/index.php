<?php

use yii\helpers\Html;
/** @var yii\web\View $this */

$this->title = 'REALFIT';
?>

<body style="background-image: url('<?= Yii::$app->urlManager->baseUrl ?>/images/home.jpg');">
    <div class="menu">
        <ul class="acceso-rapido">
            <li class="items-acceso-rapido">
                <?php
                    echo Html::a('NUEVO CLIENTE', ['clientes/createinicio']);
                ?>
            </li>
            <li class="items-acceso-rapido">
                <?php
                    echo Html::a('NUEVA SESIÓN', ['sesiones/createinicio']);
                ?>
            </li>
            <li class="items-acceso-rapido">
                <?php
                    echo Html::a('ENTRENADORES', ['entrenadorespersonales/entrenadoresclientes']);
                ?>
            </li>
            <li class="items-acceso-rapido">
                <?php
                    echo Html::a('SESIONES DE HOY', ['sesiones/versesiones']);
                ?>
            </li>
            <li class="items-acceso-rapido">
                <?php
                    echo Html::a('ESTADÍSTICAS', ['estadisticas/index']);
                ?>
            </li>
        </ul>
    </div>
</body>
