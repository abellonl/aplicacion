<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Espacios $model */
/** @var yii\widgets\ActiveForm $form */
?>

<p>
    <?= Html::a('Volver', ['volver'], ['class' => 'btn btn-secondary']) ?>
</p>

<div class="espacios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'maxcapacidad')->textInput(['maxlength' => true, 'placeholder' => 'Introduzca la capacidad máxima del espacio...', 'type' => 'number', 'id' => 'maxcapacidad-input', 'autocomplete' => 'off'])?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
