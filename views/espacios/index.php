<?php

use app\models\Espacios;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\PostSearch\Espacios $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Espacios';

?>
<div class="espacios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nuevo Espacio', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => '',
        'columns' => [
            [
                'attribute' => 'codigo',
                'value' => function($model) {
                    return 'Sala ' . $model->codigo;
                },
            ],
            'maxcapacidad',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Espacios $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo' => $model->codigo]);
                }
            ],
        ],
    ]); ?>


</div>
