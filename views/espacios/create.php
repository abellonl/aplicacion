<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Espacios $model */

$this->title = 'Nuevo Espacio';

?>
<div class="espacios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
