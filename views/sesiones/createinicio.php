<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\time\TimePicker;

/** @var yii\web\View $this */
/** @var app\models\Sesiones $model */
/** @var yii\widgets\ActiveForm $form */

$this->title = 'Crear Sesión';
?>

<h1><?= Html::encode($this->title) ?></h1>

<p>
   <?= Html::a('Volver', ['volverinicio'], ['class' => 'btn btn-secondary']) ?>
</p>

<div class="sesiones-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'codclase')->dropDownList(
                \yii\helpers\ArrayHelper::map(
                    \app\models\Clases::find()->all(),
                    'codigo',
                    function ($clase) {
                        return $clase->actividad;
                    }
                ),
                ['prompt' => 'Selecciona una clase']
            )->label('Clase') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'codespacio')->dropDownList(
                \yii\helpers\ArrayHelper::map(
                    \app\models\Espacios::find()->all(),
                    'codigo',
                    function ($espacio) {
                        return 'Sala ' . $espacio->codigo . ' (Capacidad: ' . $espacio->maxcapacidad . ' personas)';
                    }
                ),
                ['prompt' => 'Selecciona un espacio']
            )->label('Sala') ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'hora')->textInput(['placeholder' => 'Introduzca la hora (hh:mm)...', 'autocomplete' => 'off']) ?>
        </div>
        
        <div class="col-md-6">
            <?= $form->field($model, 'idmonitor')->dropDownList(
                \yii\helpers\ArrayHelper::map(
                    \app\models\Monitores::find()->all(),
                    'id',
                    function ($monitor) {
                        return $monitor->nombrecompleto;
                    }
                ),
                ['prompt' => 'Selecciona un monitor']
            )->label('Monitor') ?>
        </div>
    </div>
    
    
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
