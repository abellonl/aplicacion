<?php

use app\models\Sesiones;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\PostSearch\Sesiones $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Sesiones';

?>
<div class="sesiones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Sesión', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => '',
        'columns' => [
            [
                'label' => 'Clase', 'value' => function($model) {
                    $clase = $model->getCodclase()->one();
                    if ($clase !== null) {
                        return $clase->actividad;
                    }
                },
                'headerOptions' => ['style' => 'width: 225px;'],
            ],
            [
                'label' => 'Monitor', 'value' => function($model) {
                    $monitor = $model->getIdmonitor()->one();
                    if ($monitor !== null) {
                        return $monitor->nombrecompleto;
                    }
                },
                'headerOptions' => ['style' => 'width: 150px;'],
            ],
            [
                'attribute' => 'codespacio',
                'value' => function($model) {
                    return 'Sala ' . $model->codespacio;
                },
                'headerOptions' => ['style' => 'width: 100px;'],
            ],
            [
                'attribute' => 'hora', 
                'headerOptions' => ['style' => 'width: 100px;'], 
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Sesiones $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                },
                'headerOptions' => ['style' => 'width: 50px;'], 
            ],
        ],
    ]); ?>


</div>
