<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\PostSearch\Sesiones $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="sesiones-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'hora') ?>

    <?= $form->field($model, 'estado') ?>

    <?= $form->field($model, 'codclase') ?>

    <?= $form->field($model, 'codespacio') ?>

    <?php // echo $form->field($model, 'idmonitor') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
