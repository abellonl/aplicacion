<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Sesiones $model */

$this->title = 'Sesión';

\yii\web\YiiAsset::register($this);
?>
<div class="sesiones-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro de eliminar este registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Clase', 'value' => function($model) {
                    $clase = $model->getCodclase()->one();
                    if ($clase !== null) {
                        return $clase->actividad;
                    }
                },
                'headerOptions' => ['style' => 'width: 225px;'],
            ],
            [
                'label' => 'Monitor', 'value' => function($model) {
                    $monitor = $model->getIdmonitor()->one();
                    if ($monitor !== null) {
                        return $monitor->nombrecompleto;
                    }
                },
                'headerOptions' => ['style' => 'width: 150px;'],
            ],
            [
                'attribute' => 'codespacio',
                'value' => function($model) {
                    return 'Sala ' . $model->codespacio;
                },
                'headerOptions' => ['style' => 'width: 100px;'],
            ],
            [
                'attribute' => 'hora', 
                'headerOptions' => ['style' => 'width: 100px;'], 
            ],
        ],
    ]) ?>

</div>
