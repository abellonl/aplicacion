<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Sesiones $model */

$this->title = 'Actualizando Sesión';

?>
<div class="sesiones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
