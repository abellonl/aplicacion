<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Componen $model */

$this->title = 'Asignación de ejercicios';

\yii\web\YiiAsset::register($this);
?>
<div class="componen-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro de eliminar este registro?',
                'method' => 'post',
            ],
        ]) ?>    

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Rutina', 
                'value' => function ($model) {
                    $rutina = $model->getCodrutina()->one();
                    if ($rutina !== null) {
                        return $rutina->tipo;
                    } else {
                        return '(sin asignar)';
                    }
                },
                'headerOptions' => ['style' => 'width: 125px;'],
            ],
            [
                'label' => 'Ejercicio',
                'value' => function ($model) {
                    $ejercicio = $model->getCodejercicio()->one();
                    if ($ejercicio !== null) {
                        return $ejercicio->nombre;
                    } else {
                        return '(sin asignar)';
                    }
                },
                'headerOptions' => ['style' => 'width: 125px;'],
            ],
        ],
    ]) ?>

</div>
