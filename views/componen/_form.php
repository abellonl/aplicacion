<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Componen $model */
/** @var yii\widgets\ActiveForm $form */
?>

<p>
    <?= Html::a('Volver', ['volver'], ['class' => 'btn btn-secondary']) ?>
</p>

<div class="componen-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codrutina')->dropDownList(
        \yii\helpers\ArrayHelper::map(
            \app\models\Rutinas::find()->distinct()->all(),
            'codigo',
            function ($rutina) {
                return $rutina->tipo;
            }
        ),
        ['prompt' => 'Selecciona una rutina...']
    )->label('Rutina') ?>

    <?= $form->field($model, 'codejercicio')->dropDownList(
        \yii\helpers\ArrayHelper::map(
            \app\models\Ejercicios::find()->distinct()->all(),
            'codigo',
            function ($ejercicio) {
                return $ejercicio->nombre;
            }
        ),
        ['prompt' => 'Selecciona un ejercicio...']
    )->label('Ejercicio') ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
