<?php
/* @var $this yii\web\View */
/* @var $rutina app\models\Rutinas */
/* @var $componentes app\models\Componen[] */
/* @var $ejercicios app\models\Ejercicios[] */

use yii\helpers\Html;

?>

<?= Html::img('@web/images/logo.png', ['style' => 'float: right; width: 100px; margin: 0px;']) ?>

<h1 style="text-align: left; color: #2B2B2B;"><strong>REALFIT</strong></h1>

<h1><strong><?= Html::encode($rutina->tipo) ?></strong></h1>

<?php foreach ($ejercicios as $index => $ejercicio): ?>
    <?php if ($ejercicio->series > 0 || $ejercicio->repeticiones > 0 || $ejercicio->tiempo > 0): ?>
        <h3><?= Html::encode($ejercicio->nombre) ?></h3>
        <ul>
            <?php if ($ejercicio->series > 0): ?>
                <li><strong>Series:</strong> <?= Html::encode($ejercicio->series) ?></li>
            <?php endif; ?>
            <?php if ($ejercicio->repeticiones > 0): ?>
                <li><strong>Repeticiones:</strong> <?= Html::encode($ejercicio->repeticiones) ?></li>
            <?php endif; ?>
            <?php if ($ejercicio->tiempo > 0): ?>
                <li><strong>Tiempo:</strong> <?= Html::encode($ejercicio->tiempo) ?></li>
            <?php endif; ?>
        </ul>
    <?php endif; ?>
<?php endforeach; ?>
