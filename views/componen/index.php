<?php

use app\models\Componen;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\PostSearch\Componen $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Asignación de Ejercicios';
?>  
<div class="componen-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Asignar ejercicio', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => '',
        'columns' => [
            [
                'label' => 'Rutina', 'value' => function($model) {
                    $rutina = $model->getCodrutina()->one(); // Obtener un solo modelo en lugar de una consulta
                    if ($rutina !== null) {
                        return $rutina->tipo;
                    } else {
                        return '(sin asignar)';
                    }
                },
                'headerOptions' => ['style' => 'width: 400px;'],
            ],
            [
                'label' => 'Ejercicio', 'value' => function($model) {
                    $ejercicio = $model->getCodejercicio()->one(); // Obtener un solo modelo en lugar de una consulta
                    if ($ejercicio !== null) {
                        return $ejercicio->nombre;
                    } else {
                        return '(sin asignar)';
                    }
                },
                'headerOptions' => ['style' => 'width: 500px;'],
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Componen $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                },
                'headerOptions' => ['style' => 'width: 60px;'],
            ],
        ],
    ]); ?>


</div>
