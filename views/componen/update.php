<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Componen $model */

$this->title = 'Actualizando Asignación';

?>
<div class="componen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
