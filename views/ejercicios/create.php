<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Ejercicios $model */

$this->title = 'Nuevo Ejercicio';

?>
<div class="ejercicios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
