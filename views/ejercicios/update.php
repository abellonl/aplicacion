<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Ejercicios $model */

$this->title = 'Actualizando Ejercicio';

?>
<div class="ejercicios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
