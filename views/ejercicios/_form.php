<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Ejercicios $model */
/** @var yii\widgets\ActiveForm $form */
?>

<p>
    <?= Html::a('Volver', ['volver'], ['class' => 'btn btn-secondary']) ?>
</p>

<div class="ejercicios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Introduzca el nombre del ejercicio...', 'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'series')->textInput(['type' => 'number', 'placeholder' => 'Introduzca el número de series...', 'type' => 'number', 'id' => 'series-input', 'autocomplete' => 'off'])?>

    <?= $form->field($model, 'repeticiones')->textInput(['type' => 'number', 'placeholder' => 'Introduzca las repeticiones por serie...', 'type' => 'number', 'id' => 'repeticiones-input', 'autocomplete' => 'off'])?>

    <?= $form->field($model, 'tiempo')->textInput(['type' => 'number', 'placeholder' => 'Introduzca el tiempo...', 'type' => 'number', 'id' => 'tiempo-input', 'autocomplete' => 'off'])?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
