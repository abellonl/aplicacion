<?php

use app\models\Ejercicios;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\PostSearch\Ejercicios $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Ejercicios';

?>
<div class="ejercicios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nuevo Ejercicio', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => '',
        'columns' => [
            [
                'attribute' => 'nombre',
                'headerOptions' => ['style' => 'width: 400px;'],
            ],
            [
                'attribute' => 'series',
                'headerOptions' => ['style' => 'width: 150px;'],
            ],
            [
                'attribute' => 'repeticiones',
                'headerOptions' => ['style' => 'width: 150px;'],
            ],
            [
                'attribute' => 'tiempo',
                'headerOptions' => ['style' => 'width: 150px;'],
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Ejercicios $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo' => $model->codigo]);
                },
                'headerOptions' => ['style' => 'width: 60px;'],
            ],
        ],
    ]); ?>

a
</div>
