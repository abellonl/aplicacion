<?php

use yii\helpers\Html;
use dosamigos\highcharts\HighCharts;
use yii\grid\GridView;

// Datos para el gráfico de barras
$dataExpMonitores = [];
foreach ($monitoresData as $monitor) {
    $dataExpMonitores[] = [
        'name' => $monitor['nombrecompleto'],
        'y' => (int) $monitor['aniosexperiencia'],
    ];
}

$chartExpMonitores = [
    'chart' => ['type' => 'pie'],
    'title' => ['text' => 'AÑOS DE EXPERIENCIA DE NUESTROS MONITORES'],
    'series' => [['name' => 'Años de experiencia', 'data' => $dataExpMonitores]],
];

$dataAsistenciasClientes = [];
foreach ($asistenciaData as $asistencia) {
    $dataAsistenciasClientes[] = [
        'name' => $asistencia['nombrecompleto'],
        'y' => (int) $asistencia['Asistencias'],
    ];
}

$chartAsistenciasClientes = [
    'chart' => ['type' => 'pie'],
    'title' => ['text' => 'NUESTROS 10 CLIENTES MÁS LEALES'],
    'series' => [['name' => 'Número de asistencias', 'data' => $dataAsistenciasClientes]],
];

$dataHorasPopulares = [];
foreach ($horasMasPopularesData as $hora) {
    $dataHorasPopulares[] = [
        'name' => $hora['hora'],
        'y' => (int) $hora['total'],
    ];
}

$chartHorasPopulares = [
    'chart' => ['type' => 'pie'],
    'title' => ['text' => 'HORAS MÁS POPULARES DE LAS SESIONES'],
    'series' => [['name' => 'Número de asistentes', 'data' => $dataHorasPopulares]],
];

$actividades = [];
$totalApuntados = [];
foreach ($sesionesPopularesData as $data) {
    $actividades[] = $data['actividad'];
    $totalApuntados[] = (int) $data['total'];
}

$chartSesionesPopulares = [
    'chart' => ['type' => 'bar'],
    'title' => ['text' => 'Cantidad de Apuntados a Sesiones Populares por Actividad'],
    'xAxis' => ['categories' => $actividades, 'title' => ['text' => 'Actividades']],
    'yAxis' => ['title' => ['text' => 'Cantidad de Apuntados']],
    'series' => [['name' => 'Apuntados', 'data' => $totalApuntados]],
];

$dataTitulos = [];
foreach ($titulosPorEntrenadorData as $data) {
    $dataTitulos[] = [
        'name' => $data['nombrecompleto'],
        'y' => (int) $data['total'],
    ];
}

$chartTitulosPorEntrenador = [
    'chart' => ['type' => 'pie'],
    'title' => ['text' => 'CANTIDAD DE TÍTULOS POR ENTRENADOR PERSONAL'],
    'series' => [['name' => 'Títulos', 'data' => $dataTitulos]],
];

$clases = [];
$diferenciaAsistencia = [];

foreach ($clasesMasApuntadasMenosAsistidasData as $data) {
    $clases[] = $data['actividad'];
    $diferenciaAsistencia[] = (int) $data['total_apuntados'] - (int) $data['total_asistidos'];
}

$chartClasesMasApuntadasMenosAsistidas = [
    'chart' => ['type' => 'bar'],
    'title' => ['text' => 'Clases Más Apuntadas pero Menos Asistidas'],
    'xAxis' => ['categories' => $clases, 'title' => ['text' => 'Clases']],
    'yAxis' => ['title' => ['text' => 'Diferencia de Asistencia']],
    'series' => [['name' => 'Diferencia', 'data' => $diferenciaAsistencia]],
];
?>

<div class="site-index">
    <div class="container">
        <h1>ESTADÍSTICAS REALFIT</h1>
        <div class="row justify-content-center" style="margin-top: 64px !important;">
            <div class="col-md-6">
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <?= HighCharts::widget(['clientOptions' => $chartExpMonitores]); ?>
                        </div>
                        <div class="flip-card-back">
                            <?= GridView::widget([
                                'summary' => '',
                                'dataProvider' => new \yii\data\ArrayDataProvider([
                                    'allModels' => $monitoresData,
                                    'pagination' => false,
                                        ]),
                                'columns' => [
                                    [
                                        'attribute' => 'nombrecompleto',
                                        'label' => 'Monitor',
                                    ],
                                    [
                                        'attribute' => 'aniosexperiencia',
                                        'label' => 'Años Experiencia',
                                    ],
                                ],
                                'tableOptions' => ['class' => 'table table-striped table-bordered'],
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <?= HighCharts::widget(['clientOptions' => $chartAsistenciasClientes]); ?>
                        </div>
                        <div class="flip-card-back">
                            <?=
                            GridView::widget([
                                'summary' => '',
                                'dataProvider' => new \yii\data\ArrayDataProvider([
                                    'allModels' => $asistenciaData,
                                    'pagination' => false,
                                        ]),
                                'columns' => [
                                    [
                                        'attribute' => 'nombrecompleto',
                                        'label' => 'Cliente',
                                    ],
                                    [
                                        'attribute' => 'Asistencias',
                                        'label' => 'Número de Asistencias',
                                    ],
                                ],
                                'tableOptions' => ['class' => 'table table-striped table-bordered'],
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row justify-content-center" style="margin-top: 20px !important;">
            <div class="col-md-6">
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <?= HighCharts::widget(['clientOptions' => $chartSesionesPopulares]); ?>
                        </div>
                        <div class="flip-card-back">
                            <?= GridView::widget([
                                'summary' => '',
                                'dataProvider' => new \yii\data\ArrayDataProvider([
                                    'allModels' => $sesionesPopularesData,
                                    'pagination' => false,
                                ]),
                                'columns' => [
                                    [
                                        'attribute' => 'actividad',
                                        'label' => 'Sesion',
                                    ],
                                    [
                                        'attribute' => 'total',
                                        'label' => 'Número de Asistentes',
                                    ],
                                ],
                                'tableOptions' => ['class' => 'table table-striped table-bordered'],
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <?= HighCharts::widget(['clientOptions' => $chartHorasPopulares]); ?>
                        </div>
                        <div class="flip-card-back">
                            <?= GridView::widget([
                                'summary' => '',
                                'dataProvider' => new \yii\data\ArrayDataProvider([
                                    'allModels' => $horasMasPopularesData,
                                    'pagination' => false,
                                ]),
                                'columns' => [
                                    [
                                        'attribute' => 'hora',
                                        'label' => 'Hora',
                                        'value' => function ($model) {
                                            return date('H:i', strtotime($model['hora']));
                                        },
                                    ],
                                    [
                                        'attribute' => 'total',
                                        'label' => 'Número de Asistentes',
                                    ],
                                ],
                                'tableOptions' => ['class' => 'table table-striped table-bordered'],
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
        <div class="row justify-content-center" style="margin-top: 20px !important;">
            <div class="col-md-6">
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <?= HighCharts::widget(['clientOptions' => $chartTitulosPorEntrenador]); ?>
                        </div>
                        <div class="flip-card-back">
                            <?= GridView::widget([
                                'summary' => '',
                                'dataProvider' => new \yii\data\ArrayDataProvider([
                                    'allModels' => $titulosPorEntrenadorData,
                                    'pagination' => false,
                                        ]),
                                'columns' => [
                                    [
                                        'attribute' => 'nombrecompleto',
                                        'label' => 'Monitor',
                                    ],
                                    [
                                        'attribute' => 'total',
                                        'label' => 'Número de títulos',
                                    ],
                                ],
                                'tableOptions' => ['class' => 'table table-striped table-bordered'],
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <?= HighCharts::widget(['clientOptions' => $chartClasesMasApuntadasMenosAsistidas]); ?>
                        </div>
                        <div class="flip-card-back">
                            <?= GridView::widget([
                                'summary' => '',
                                'dataProvider' => new \yii\data\ArrayDataProvider([
                                    'allModels' => $clasesMasApuntadasMenosAsistidasData,
                                    'pagination' => false,
                                        ]),
                                'columns' => [
                                    [
                                        'attribute' => 'actividad',
                                        'label' => 'Sesion',
                                    ],
                                    [
                                        'attribute' => 'total_apuntados',
                                        'label' => 'Clientes Apuntados',
                                    ],
                                    [
                                        'attribute' => 'total_asistidos',
                                        'label' => 'Asistencias',
                                    ],
                                ],
                                'tableOptions' => ['class' => 'table table-striped table-bordered'],
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
            
            
        </div>
    </div>
</div>


