<?php

use app\models\Rutinas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\PostSearch\Rutinas $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Rutinas';

?>
<div class="rutinas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nueva Rutina', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => '',
        'columns' => [
            [
                'attribute' => 'tipo',
                'headerOptions' => ['style' => 'width: 400px;'],
            ],
            [
                'label' => 'Entrenador Personal', 'value' => function($model) {
                    $entrenadorPersonal = $model->getIdentrenadorpersonal()->one();
                    if ($entrenadorPersonal !== null) {
                        return $entrenadorPersonal->nombrecompleto;
                    } else {
                        return '(sin asignar)';
                    }
                },
                'headerOptions' => ['style' => 'width: 600px;'],
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Rutinas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo' => $model->codigo]);
                },
                'headerOptions' => ['style' => 'width: 60px;'],
            ],
        ],
    ]); ?>


</div>
