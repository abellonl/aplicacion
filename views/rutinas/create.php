<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Rutinas $model */

$this->title = 'Crear Rutina';

?>
<div class="rutinas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
