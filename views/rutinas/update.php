<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Rutinas $model */

$this->title = 'Actualizando Rutina';

?>
<div class="rutinas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
