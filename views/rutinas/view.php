<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Rutinas $model */

$this->title = $model->tipo;

\yii\web\YiiAsset::register($this);
?>
<div class="rutinas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Descargar rutina', ['componen/descargar','codigo' => $model->codigo], [
            'class'=>'btn btn-info',
            'target'=>'_blank',
            'data-toogle'=>'tooltip',
            'title'=>'Descargar rutina en PDF'
        ]) ?>
        <?= Html::a('Actualizar', ['update', 'codigo' => $model->codigo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'codigo' => $model->codigo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro de eliminar este registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tipo',
            [
                'label' => 'Entrenador Personal', 'value' => function($model) {
                    $entrenadorPersonal = $model->getIdentrenadorpersonal()->one();
                    if ($entrenadorPersonal !== null) {
                        return $entrenadorPersonal->nombrecompleto;
                    } else {
                        return '(sin asignar)';
                    }
                },
                'headerOptions' => ['style' => 'width: 600px;'],
            ],
        ],
    ]) ?>

</div>
