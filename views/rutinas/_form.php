<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Rutinas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<p>
    <?= Html::a('Volver', ['volver'], ['class' => 'btn btn-secondary']) ?>
</p>

<div class="rutinas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tipo')->textInput(['maxlength' => true, 'placeholder' => 'Ingrese el nombre de la rutina...', 'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'identrenadorpersonal')->dropDownList(
        \yii\helpers\ArrayHelper::map(
            \app\models\Entrenadorespersonales::find()->all(),
            'id',
            function ($entrenador) {
                return $entrenador->nombrecompleto;
            }
        ),
        ['prompt' => 'Selecciona un entrenador personal']
    )->label('Entrenador Personal') ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
