<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Apuntarse $model */
/** @var yii\widgets\ActiveForm $form */
/** @var app\models\Clases $clase */

$this->title = 'Apuntar Cliente';
?>

<p>
    <?= Html::a('Volver', ['volverinicio'], ['class' => 'btn btn-secondary']) ?>
</p>

<div class="apuntarse-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idcliente')->dropDownList(
        \yii\helpers\ArrayHelper::map(
            \app\models\Clientes::find()->all(),
            'id',
            function ($cliente) {
                return $cliente->nombrecompleto;
            }
        ),
        ['prompt' => 'Selecciona un cliente']
    )->label('Cliente') ?>
    
    <?= $form->field($model, 'asistencia')->textInput(['placeholder' => 'Asistió el cliente: SI/NO...', 'id' => 'asistencia-input', 'autocomplete' => 'off']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
