<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Apuntarse $model */

$this->title = 'Apunte de Cliente';

\yii\web\YiiAsset::register($this);
?>
<div class="apuntarse-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'codigo' => $model->codigo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'codigo' => $model->codigo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro de eliminar este registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Cliente',
                'value' => function ($model) {
                    $cliente = $model->getIdcliente()->one();
                        return $cliente->nombrecompleto;
                },
            ],
            [
                'label' => 'Clase',
                'value' => function ($model) {
                    $clase = $model->getCodclase()->one();
                        return $clase->actividad;
                },
            ],
            [
                'label' => 'Hora',
                'value' => function ($model) {
                    $hora = $model->getIdsesion()->one();
                        return $hora->hora;
                },
            ],
            [
                'attribute' => 'asistencia', 'value' => function($model) {
                    return $model->asistencia ? 'Asistió' : 'Sin registro';
                },
            ],
        ],
    ]) ?>

</div>
