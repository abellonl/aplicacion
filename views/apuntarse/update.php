<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Apuntarse $model */

$this->title = 'Actualizando Apunte';

?>
<div class="apuntarse-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
