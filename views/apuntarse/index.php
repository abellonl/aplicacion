<?php

use app\models\Apuntarse;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\PostSearch\Apuntarse $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Clientes Apuntados';

?>
<div class="apuntarse-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Apuntar Cliente', ['versesiones'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => '',
        'columns' => [
            [
                'label' => 'Cliente',
                'value' => function ($model) {
                    $cliente = $model->getIdcliente()->one();
                        return $cliente->nombrecompleto;
                },
                'headerOptions' => ['style' => 'width: 175px;'],
            ],
            [
                'label' => 'Clase',
                'value' => function ($model) {
                    $clase = $model->getCodclase()->one();
                        return $clase->actividad;
                },
                'headerOptions' => ['style' => 'width: 200px;'],
            ],
            [
                'label' => 'Hora',
                'value' => function ($model) {
                    $hora = $model->getIdsesion()->one();
                        return $hora->hora;
                },
                'headerOptions' => ['style' => 'width: 150px;'],
            ],
            [
                'attribute' => 'asistencia', 'value' => function($model) {
                    return $model->asistencia ? 'Asistió' : 'Sin registro';
                },
                'headerOptions' => ['style' => 'width: 150px;'], 
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Apuntarse $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo' => $model->codigo]);
                },
                'headerOptions' => ['style' => 'width: 60px;'],
            ],
        ],
    ]); ?>


</div>
