/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/ClientSide/javascript.js to edit this template
 */

/*validar que no se pueda poner la letra e en el los type number porque la detecta como la e matematica*/
$(document).ready(function() {
    $('#telefono-input').on('input', function() {
        $(this).val($(this).val().replace('e', ''));
    });
});

$(document).ready(function() {
    $('#series-input').on('input', function() {
        $(this).val($(this).val().replace('e', ''));
    });
});

$(document).ready(function() {
    $('#repeticiones-input').on('input', function() {
        $(this).val($(this).val().replace('e', ''));
    });
});

$(document).ready(function() {
    $('#tiempo-input').on('input', function() {
        $(this).val($(this).val().replace('e', ''));
    });
});

$(document).ready(function() {
    $('#maxcapacidad-input').on('input', function() {
        $(this).val($(this).val().replace('e', ''));
    });
});

$(document).ready(function() {
    $('#aniosexperiencia-input').on('input', function() {
        $(this).val($(this).val().replace('e', ''));
    });
});



// cambiar la clase del encabezado al hacer scroll
window.addEventListener('scroll', function() {
    var header = document.querySelector('.navbar-expand-md');
    if (window.scrollY > 50) { // Puedes ajustar el valor de 50 a la cantidad de scroll que prefieras
        header.classList.add('scrolled');
    } else {
        header.classList.remove('scrolled');
    }
});


/*
 * ESTILOS BUSCADORES DEL GRIDVIEW
 */

/*CLIENTES*/
document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('.clientes-index #w0-filters > td:nth-child(1) > input');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Introduzca DNI/NIE...');
        inputElement.style.display = 'block'; // Si deseas mostrar el input nuevamente
    }
});

document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('.clientes-index #w0-filters > td:nth-child(2) > input');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Introduzca el nombre...');
        inputElement.style.display = 'block'; // Si deseas mostrar el input nuevamente
    }
});


/*ENTRENADORES*/
document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('.entrenadorespersonales-index #w0-filters > td:nth-child(1) > input');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Introduzca DNI/NIE...');
        inputElement.style.display = 'block'; // Si deseas mostrar el input nuevamente
    }
});

document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('.entrenadorespersonales-index #w0-filters > td:nth-child(2) > input');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Introduzca el nombre...');
        inputElement.style.display = 'block'; // Si deseas mostrar el input nuevamente
    }
});


/*MONITORES*/
document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('.monitores-index #w0-filters > td:nth-child(1) > input');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Introduzca DNI/NIE...');
        inputElement.style.display = 'block'; // Si deseas mostrar el input nuevamente
    }
});

document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('.monitores-index #w0-filters > td:nth-child(2) > input');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Introduzca el nombre...');
        inputElement.style.display = 'block'; // Si deseas mostrar el input nuevamente
    }
});


/*APUNTARSE*/
document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('.apuntarse-index #w0-filters > td:nth-child(1) > input');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Introduzca el código...');
        inputElement.style.display = 'block'; // Si deseas mostrar el input nuevamente
    }
});

document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('.apuntarse-index #w0-filters > td:nth-child(2) > input');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'SI / NO');
        inputElement.style.display = 'block'; // Si deseas mostrar el input nuevamente
    }
});


/*EJERCICIOS*/
document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('.ejercicios-index #w0-filters > td:nth-child(1) > input');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Introduzca el nombre...');
        inputElement.style.display = 'block'; // Si deseas mostrar el input nuevamente
    }
});


/*ESPACIOS*/
document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('.espacios-index #w0-filters > td:nth-child(1) > input');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Introduzca el número de la sala...');
        inputElement.style.display = 'block'; // Si deseas mostrar el input nuevamente
    }
});


/*SESIONES*/
document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('.sesiones-index #w0-filters > td:nth-child(3) > input');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Introduzca la sala...');
        inputElement.style.display = 'block'; // Si deseas mostrar el input nuevamente
    }
});

document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('.sesiones-index #w0-filters > td:nth-child(4) > input');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Introduzca la hora...');
        inputElement.style.display = 'block'; // Si deseas mostrar el input nuevamente
    }
});

document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('.sesiones-index #w0-filters > td:nth-child(5) > input');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Completa: Si / No');
        inputElement.style.display = 'block'; // Si deseas mostrar el input nuevamente
    }
});


/*TÍTULOS*/
document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('.titulos-index #w0-filters > td:nth-child(1) > input');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Introduzca el nombre...');
        inputElement.style.display = 'block'; // Si deseas mostrar el input nuevamente
    }
});

/*RUTINAS*/
document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('.rutinas-index #w0-filters > td:nth-child(1) > input');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Introduzca el nombre...');
        inputElement.style.display = 'block'; // Si deseas mostrar el input nuevamente
    }
});

/*CLASES*/
document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('.clases-index #w0-filters > td:nth-child(1) > input');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Introduzca el código...');
        inputElement.style.display = 'block'; // Si deseas mostrar el input nuevamente
    }
});

document.addEventListener('DOMContentLoaded', function() {
    const inputElement = document.querySelector('.clases-index #w0-filters > td:nth-child(2) > input');
    if (inputElement) {
        inputElement.setAttribute('placeholder', 'Introduzca la actividad...');
        inputElement.style.display = 'block'; // Si deseas mostrar el input nuevamente
    }
});


/*
 * Cambiar inputs asistencia cliente si(0) y no(1)
 */
document.querySelector('form').addEventListener('submit', function(event) {
    let inputField = document.getElementById('asistencia-input');
    let inputValue = inputField.value.toLowerCase();
    
    if (inputValue === 'si') {
        inputField.value = 0;
    } else if (inputValue === 'no') {
        inputField.value = 1;
    }
});

