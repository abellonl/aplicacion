<?php

namespace app\controllers;

use app\models\Monitores;
use app\models\Apuntarse;
use app\models\Titulos;
use yii\web\Controller;


class EstadisticasController extends Controller
{
    
    /**
     * Grafico barras
     * Años EXP Monitores
     */
    public function actionIndex() {
        $monitoresData = Monitores::find()
            ->select(['nombrecompleto', 'aniosexperiencia'])
            ->asArray()
            ->all();

        
        $asistenciaData = Apuntarse::find()
            ->select(['Apuntarse.idcliente', 'COUNT(*) AS Asistencias', 'Clientes.nombrecompleto'])
            ->innerJoin('Clientes', 'Apuntarse.idcliente = Clientes.id')
            ->where(['Apuntarse.asistencia' => true])
            ->groupBy('Apuntarse.idcliente')
            ->orderBy(['Asistencias' => SORT_DESC])
            ->limit(10)
            ->asArray()
            ->all();

        $horasMasPopularesData = Apuntarse::find()
            ->select(['sesiones.hora', 'COUNT(*) AS total'])
            ->innerJoin('sesiones', 'Apuntarse.idsesion = sesiones.id')
            ->where(['Apuntarse.asistencia' => true])
            ->groupBy(['sesiones.hora'])
            ->orderBy(['total' => SORT_DESC])
            ->limit(5)
            ->asArray()
            ->all();
        
        $sesionesPopularesData = Apuntarse::find()
            ->select(['sesiones.id', 'clases.actividad', 'COUNT(Apuntarse.idcliente) AS total'])
            ->innerJoin('sesiones', 'Apuntarse.idsesion = sesiones.id')
            ->innerJoin('clases', 'sesiones.codclase = clases.codigo')
            ->groupBy('sesiones.id, clases.actividad')
            ->orderBy(['total' => SORT_DESC])
            ->limit(5)
            ->asArray()
            ->all();
        
        $titulosPorEntrenadorData = Titulos::find()
            ->select(['entrenadorespersonales.nombrecompleto', 'COUNT(Titulos.codigo) AS total'])
            ->innerJoin('entrenadorespersonales', 'Titulos.identrenadorpersonal = entrenadorespersonales.id')
            ->groupBy('entrenadorespersonales.nombrecompleto')
            ->orderBy(['total' => SORT_DESC])
            ->asArray()
            ->all();
        
        $clasesMasApuntadasMenosAsistidasData = Apuntarse::find()
            ->select(['clases.actividad', 'COUNT(Apuntarse.idcliente) AS total_apuntados', 'COUNT(CASE WHEN Apuntarse.asistencia = 1 THEN 1 END) AS total_asistidos'])
            ->innerJoin('sesiones', 'Apuntarse.idsesion = sesiones.id')
            ->innerJoin('clases', 'sesiones.codclase = clases.codigo')
            ->groupBy('clases.actividad')
            ->orderBy(['total_apuntados' => SORT_DESC])
            ->limit(5)
            ->asArray()
            ->all();
        
        
        return $this->render('estadisticas', [
            'monitoresData' => $monitoresData,
            'asistenciaData' => $asistenciaData,
            'horasMasPopularesData' => $horasMasPopularesData,
            'sesionesPopularesData' => $sesionesPopularesData,
            'titulosPorEntrenadorData' => $titulosPorEntrenadorData,
            'clasesMasApuntadasMenosAsistidasData' => $clasesMasApuntadasMenosAsistidasData,
        ]);
        
    }
    
    
}