<?php

namespace app\controllers;

use app\models\Entrenadorespersonales;
use app\models\PostSearch\Entrenadorespersonales as EntrenadorespersonalesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * EntrenadorespersonalesController implements the CRUD actions for Entrenadorespersonales model.
 */
class EntrenadorespersonalesController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Entrenadorespersonales models.
     *
     * @return string
     */
    public function actionIndex() {
        $searchModel = new EntrenadorespersonalesSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Entrenadorespersonales model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }

    /**
     * Creates a new Entrenadorespersonales model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $model = new Entrenadorespersonales();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {

                if ($model->save()) {
                    return $this->redirect(['titulos/create', 'id' => $model->id]);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Entrenadorespersonales model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if ($model->save()) {
                return $this->redirect(['titulos/update', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Entrenadorespersonales model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        // Encuentra el entrenador con menos clientes asignados
        $entrenadorMenosClientes = Entrenadorespersonales::find()
            ->leftJoin('clientes', 'entrenadorespersonales.id = clientes.identrenadorpersonal')
            ->groupBy('entrenadorespersonales.id')
            ->orderBy('COUNT(clientes.id)')
            ->one();

        // Actualiza los clientes asociados al entrenador que se está eliminando
        \app\models\Clientes::updateAll(['identrenadorpersonal' => $entrenadorMenosClientes->id], ['identrenadorpersonal' => $id]);
        
        // Actualiza las rutinas asociadas al entrenador que se está eliminando
        \app\models\Rutinas::updateAll(['identrenadorpersonal' => null], ['identrenadorpersonal' => $id]);

        // Elimina el entrenador
        $entrenador = $this->findModel($id);
        $entrenador->delete();

        return $this->redirect(['index']);
    }



    public function actionVolver()
    {
        return $this->redirect(['index']);
    }

    /**
     * Finds the Entrenadorespersonales model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Entrenadorespersonales the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Entrenadorespersonales::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('La página no existe.');
    }

    /* Método para mostrar el ListView */

    public function actionEntrenadoresclientes() {
        $dataProvider = new ActiveDataProvider([
            'query' => Entrenadorespersonales::find(),
            'pagination' => false,
        ]);

        return $this->render('/listviews/entrenadoresclientes', [
            'dataProvider' => $dataProvider,
        ]);
    }

    // Método para obtener los clientes de un entrenador específico
    public function actionView($id) {
        $model = Entrenadorespersonales::findOne($id);
        if ($model === null) {
            throw new \yii\web\NotFoundHttpException('El entrenador no existe.');
        }

        $clientes = $model->clientes; // Usar la relación definida en el modelo

        return $this->render('view', [
                    'model' => $model,
                    'clientes' => $clientes,
        ]);
    }

}
