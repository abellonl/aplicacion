<?php

namespace app\controllers;

use app\models\Componen;
use app\models\Rutinas;
use app\models\Ejercicios;
use app\models\PostSearch\Componen as ComponenSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;
use Yii;


/**
 * ComponenController implements the CRUD actions for Componen model.
 */
class ComponenController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Componen models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ComponenSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Componen model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Componen model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Componen();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Componen model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Componen model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionVolver()
    {
        return $this->redirect(['index']);
    }
    
    /**
     * Finds the Componen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Componen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Componen::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('La página no existe.');
    }   
    
    public function actionDescargar($codigo)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'application/pdf');

        // Obtener los códigos de los ejercicios relacionados con la rutina
        $componentes = Componen::find()
            ->where(['codrutina' => $codigo])
            ->all();

        // Obtener la rutina específica
        $rutina = Rutinas::findOne($codigo);
        if (!$rutina) {
            throw new NotFoundHttpException('Rutina no encontrada');
        }

        // Obtener los ejercicios completos
        $ejercicios = [];
        foreach ($componentes as $componente) {
            $ejercicio = Ejercicios::findOne($componente->codejercicio);
            if ($ejercicio !== null) {
                $ejercicios[] = $ejercicio;
            }
        }

        // Verificar si se encontraron los ejercicios
        if (empty($ejercicios)) {
            throw new NotFoundHttpException('Ejercicios no encontrados');
        }

        // Configurar el PDF
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('descargaPDF', [
                'rutina' => $rutina,
                'componentes' => $componentes,
                'ejercicios' => $ejercicios,
            ]),
            'methods' => [
                'SetTitle' => 'Descargar rutina',
                'SetFooter' => ['|Página {PAGENO}|'],
            ]
        ]);

        return $pdf->render();
    }

    
}