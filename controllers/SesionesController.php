<?php

namespace app\controllers;

use app\models\Sesiones;
use app\models\PostSearch\Sesiones as SesionesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use Yii;
use yii\helpers\Url;

/**
 * SesionesController implements the CRUD actions for Sesiones model.
 */
class SesionesController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Sesiones models.
     *
     * @return string
     */
    public function actionIndex() {
        $searchModel = new SesionesSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sesiones model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }

    /**
     * Creates a new Sesiones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Sesiones();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save(false)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    
    public function actionCreateinicio() {
        $model = new Sesiones();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save(false)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('createinicio', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Sesiones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sesiones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionVolver()
    {
        return $this->redirect(['index']);
    }
    
    public function actionVolverinicio()
    {
        // Obtener la URL de la página anterior
        $previousUrl = Url::previous('previous') ?: ['site/index']; // Si no hay página anterior, redirige a una página por defecto

        // Redirigir a la página anterior
        return $this->redirect($previousUrl);
    }

    /**
     * Finds the Sesiones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Sesiones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Sesiones::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /* Método para mostrar el ListView */

    public function actionVersesiones() {
        $dataProvider = new ActiveDataProvider([
            'query' => Sesiones::find(),
            'pagination' => false,
        ]);

        return $this->render('/listviews/versesiones', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        $model = Sesiones::findOne($id);
        if ($model === null) {
            throw new \yii\web\NotFoundHttpException('La sesión no existe.');
        }

        $apuntarse = $model->apuntarse;
        $clases = $model->codclase;
        $espacios = $model->codespacio;
        $monitores = $model->idmonitor;

        return $this->render('view', [
                    'model' => $model,
                    'apuntarse' => $apuntarse,
                    'clases' => $clases,
                    'espacios' => $espacios,
                    'monitores' => $monitores,
        ]);
    }

}
