<?php

namespace app\controllers;

use app\models\Titulos;
use app\models\Entrenadorespersonales;
use app\models\PostSearch\Titulos as TitulosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;

/**
 * TitulosController implements the CRUD actions for Titulos model.
 */
class TitulosController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Titulos models.
     *
     * @return string
     */
    public function actionIndex() {
        $searchModel = new TitulosSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Titulos model.
     * @param int $codigo Codigo
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo) {
        return $this->render('view', [
                    'model' => $this->findModel($codigo),
        ]);
    }

    /**
     * Creates a new Titulos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
//    public function actionCreate()
//    {
//        $model = new Titulos();
//
//        if ($this->request->isPost) {
//            if ($model->load($this->request->post()) && $model->save()) {
//                return $this->redirect(['view', 'codigo' => $model->codigo]);
//            }
//        } else {
//            $model->loadDefaultValues();
//        }
//
//        return $this->render('create', [
//            'model' => $model,
//        ]);
//    }

    public function actionCreate($id) {
        $entrenador = Yii::$app->session->get('entrenador');

        $model = new Titulos();
        $model->identrenadorpersonal = $id;

        if ($this->request->isPost) {
            $titulosPost = $this->request->post('Titulos')['titulo'];
            $success = true;
            
            if ($success) {
                foreach ($titulosPost as $titulo) {
                    if (!empty($titulo)) {
                        $nuevoTitulo = new Titulos();
                        $nuevoTitulo->identrenadorpersonal = $id;
                        $nuevoTitulo->titulo = $titulo;
                        if (!$nuevoTitulo->save()) {
                            $success = false;
                        }
                    }
                }

                if ($success) {
                    Yii::$app->session->remove('entrenador');
                    return $this->redirect(['entrenadorespersonales/view', 'id' => $id]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Titulos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigo Codigo
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $entrenador = Entrenadorespersonales::findOne($id);
        $titulos = Titulos::findAll(['identrenadorpersonal' => $id]);

        if ($this->request->isPost) {
            $titulosPost = $this->request->post('Titulos');
            $success = true;

            if ($success) {
                foreach ($titulos as $index => $titulo) {
                    if (isset($titulosPost[$index]['titulo'])) {
                        if ($titulosPost[$index]['titulo'] === '') {
                            // Si el título está vacío, eliminar el registro
                            $titulo->delete();
                        } else {
                            $titulo->titulo = $titulosPost[$index]['titulo'];
                            if (!$titulo->save()) {
                                $success = false;
                            }
                        }
                    } else {
                        $titulo->delete();
                    }
                }

                for ($i = count($titulos); $i < count($titulosPost); $i++) {
                    if (!empty($titulosPost[$i]['titulo'])) {
                        $titulonuevo = new Titulos();
                        $titulonuevo->identrenadorpersonal = $id;
                        $titulonuevo->titulo = $titulosPost[$i]['titulo'];
                        if (!$titulonuevo->save()) {
                            $success = false;
                            $titulos[] = $titulonuevo;
                        }
                    }
                }

                if ($success) {
                    return $this->redirect(['entrenadorespersonales/view', 'id' => $id]);
                }
            }
        }

        return $this->render('update', [
                    'model' => $entrenador,
                    'titulos' => $titulos,
        ]);
    }

    /**
     * Deletes an existing Titulos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigo Codigo
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo) {
        $this->findModel($codigo)->delete();

        return $this->redirect(['index']);
    }

    public function actionVolver()
    {
        return $this->redirect(['index']);
    }

    /**
     * Finds the Titulos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigo Codigo
     * @return Titulos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo) {
        if (($model = Titulos::findOne(['codigo' => $codigo])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
